﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TalonRO_SQI_Item_Description_Updater
{
    class Program
    {
        static bool addArrowCraftingInfo;
        static bool addBrewingInfo;
        static bool addCookingInfo;
        static bool addBuyableInfo;
        static bool addDyestuffInfo;
        static bool addForgingInfo;
        static bool addNewWorldFoodInfo;
        static bool addQuestItemInfo;
        static bool addSQIInfo;
        static bool addSQIBonusInfo;
        static bool addSQIIngredientInfo;
        static bool addSeasonalInfo;
        static bool addSkillRequirementInfo;

        static string OldSQIIngredientInfo = @"^0000FFSQI Ingredient^000000";
        static string OldBuyableInfo = @"^FF0000[NPC-buyable item]^000000";
        static string OldNewBuyableInfo = @"^FF0000[NPC Buyable]^000000";

        static string NewArrowCraftingInfo = @"^C51162[Arrow]^000000";
        static string NewBrewingInfo = @"^AA00FF[Brewing]^000000";
        static string NewCookingInfo = @"^6200EA[Cooking]^000000";
        static string NewBuyableInfo = @"^FF0000[Buyable]^000000";
        static string NewDyestuffsInfo = @"^2962FF[Dyestuffs]^000000";
        static string NewForgingInfo = @"^0091EA[Forging]^000000";
        static string NewNewWorldFoodInfo = @"^00B8D4[New World Food]^000000";
        static string NewQuestInfo = @"^00BFA5[Quest]^000000";
        static string NewSQIInfo = @"^FF6D00[SQI]^000000";
        static string NewSQIBonusInfo = @"^009900[SQI Bonus]^000000";
        static string NewSQIIngredientInfo = @"^0000FF[SQI Ingredient]^000000";
        static string NewSeasonalEventInfo = @"^FFAB00[Seasonal]^000000";
        static string NewSkillRequirementInfo = @"^455A64[Skill]^000000";

        static List<int> ArrowCrafting;
        static List<int> Brewing;
        static List<int> Buyable;
        static List<int> Cooking;
        static List<int> Dyestuffs;
        static List<int> Forging;
        static List<int> NewWorldFood;
        static List<int> Quest;
        static List<int> SQI;
        static List<int> SQIBonus;
        static List<int> SQIIngredient;
        static List<int> SeasonalEvent;
        static List<int> SkillRequirement;

        static string BaseDirectory = AppDomain.CurrentDomain.BaseDirectory;

        static Encoding FileEncoding = Encoding.GetEncoding( 1252 );

        static string OutputFileName = @"idnum2itemdesctable.txt";

        static void Main( string[] args )
        {
            Console.WriteLine( "TalonRO Item Description Updater\n" );

            string inputFile = "";

            if( args.Length == 1 )
            {
                inputFile = args[0];
            }

#if DEBUG
            if( string.IsNullOrWhiteSpace( inputFile ) )
            {
                inputFile = @"../../idnum2itemdesctable_test.txt";
            }
#endif
            var outputFilePath = Path.Combine( BaseDirectory, OutputFileName );

            if( !string.IsNullOrWhiteSpace( inputFile ) )
            {
                Console.WriteLine( "Input file: " + Path.GetFullPath( inputFile ) );
            }
            else
            {
                Console.WriteLine( "Drag & drop the original description file onto the exe." );
                Console.ReadLine();
                return;
            }

            Console.WriteLine();
            ReadUserInput();

            Console.WriteLine( "\nLoading IDs." );
            LoadData();

            Console.WriteLine( "Loading description file." );
            var input = File.ReadAllLines( inputFile, FileEncoding ).ToList();

            Console.WriteLine( "Creating backup file." );
            CreateInputBackup( inputFile );

            Console.WriteLine( "Removing existing info." );
            var output = RemoveExistingInfo( input );

            Console.WriteLine( "Adding new info." );
            output = AddNewInfo( output );

            Console.WriteLine( "Output file: " + outputFilePath );
            File.WriteAllLines( outputFilePath, output, FileEncoding );

            Console.WriteLine( "\nDone!" );
            Console.ReadLine();
        }

        private static void ReadUserInput()
        {
            addArrowCraftingInfo = ReadConsoleLine( "Do you want to include the Arrow Crafting info ? ( y / n )" );
            addBrewingInfo = ReadConsoleLine( "Do you want to include the Brewing info ? ( y / n )" );
            addBuyableInfo = ReadConsoleLine( "Do you want to include the Buyable info ? ( y / n )" );
            addCookingInfo = ReadConsoleLine( "Do you want to include the Cooking info ? ( y / n )" );
            addDyestuffInfo = ReadConsoleLine( "Do you want to include the Dyestuff info ? ( y / n )" );
            addForgingInfo = ReadConsoleLine( "Do you want to include the Forging info ? ( y / n )" );
            addNewWorldFoodInfo = ReadConsoleLine( "Do you want to include the New World Food info ? ( y / n )" );
            addQuestItemInfo = ReadConsoleLine( "Do you want to include the Quest Item info ? ( y / n )" );
            addSQIInfo = ReadConsoleLine( "Do you want to include the SQI info ? ( y / n )" );
            addSQIBonusInfo = ReadConsoleLine( "Do you want to include the SQI Bonus info ? ( y / n )" );
            addSQIIngredientInfo = ReadConsoleLine( "Do you want to include the SQI Ingredient info ? ( y / n )" );
            addSeasonalInfo = ReadConsoleLine( "Do you want to include the Seasonal info ? ( y / n )" );
            addSkillRequirementInfo = ReadConsoleLine( "Do you want to include the Skill Requirement info ? ( y / n )" );
        }

        private static bool ReadConsoleLine( string text )
        {
            Console.WriteLine( text );
            string userInput = Console.ReadLine().Trim().ToLower();

            return ( userInput == "y" );
        }

        private static void LoadData()
        {
            // Loading data directly into memory so I dont have to depend on any files

            // List is generated from Iela's List
            ArrowCrafting = new List<int>() { 603, 604, 609, 713, 714, 715, 716, 717, 724, 733, 756, 757, 902, 904, 909, 910,
                911, 912, 913, 920, 922, 923, 937, 939, 947, 952, 956, 957, 958, 959, 968, 969, 984, 985, 990, 991, 992, 993,
                994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1003, 1010, 1011, 1018, 1019, 1021, 1027, 1031, 1035, 1038,
                1041, 1043, 1044, 1053, 1063, 1064, 1066, 1067, 1068, 1095, 1098, 2257, 2281, 2288, 2292, 2319, 2329, 2332,
                2333, 2408, 2618, 5014, 7002, 7008, 7010, 7018, 7019, 7020, 7021, 7022, 7023, 7024, 7026, 7027, 7035, 7036,
                7066, 7067, 7068, 7069, 7097, 7098, 7100, 7108, 7109, 7120, 7123, 7155, 7163, 7164, 7172, 7210, 7221, 7263,
                7315, 7316, 7318, 7319, 7321, 7323, 7340, 7435, 7442, 7445, 7446, 7447, 7448, 7450, 7451, 7510, 7511, 7561,
                7562, 7566, 7752, 7753 };

            // List is generated from Iela's List
            Brewing = new List<int>() { 501, 503, 504, 507, 508, 509, 510, 518, 704, 708, 713, 715, 716, 717, 905, 911, 929,
                950, 952, 970, 1012, 1017, 1032, 1044, 1050, 1051, 1057, 1059, 1061, 1092, 1093, 7033, 7126, 7140, 7141, 7143 };

            // List is generated from idnum2itemdesctable.txt
            Buyable = new List<int>() { 501, 502, 503, 504, 506, 507, 511, 512, 513, 514, 515, 516, 517, 518, 519, 522,
                525, 528, 531, 532, 535, 536, 537, 554, 577, 579, 580, 581, 601, 602, 610, 611, 612, 613, 614, 615, 643, 645,
                656, 657, 711, 712, 713, 715, 716, 717, 732, 734, 735, 736, 737, 740, 741, 742, 744, 745, 746, 747, 748, 902,
                905, 910, 911, 912, 934, 952, 964, 986, 987, 988, 1002, 1010, 1011, 1017, 1025, 1032, 1050, 1051, 1052, 1059,
                1062, 1065, 1092, 1093, 1101, 1104, 1107, 1110, 1113, 1116, 1119, 1122, 1123, 1126, 1129, 1146, 1151, 1154,
                1157, 1160, 1201, 1204, 1207, 1210, 1213, 1216, 1219, 1222, 1243, 1245, 1247, 1248, 1249, 1250, 1252, 1254,
                1301, 1351, 1354, 1357, 1360, 1401, 1404, 1407, 1410, 1451, 1454, 1457, 1460, 1463, 1501, 1504, 1507, 1510,
                1513, 1516, 1519, 1522, 1601, 1604, 1607, 1610, 1617, 1619, 1701, 1704, 1707, 1710, 1713, 1714, 1718, 1721,
                1750, 1751, 1752, 1770, 1771, 1773, 1774, 1801, 1803, 1805, 1807, 1809, 1811, 1901, 1903, 1905, 1907, 1909,
                1911, 1950, 1952, 1954, 1956, 1958, 1960, 2101, 2103, 2105, 2107, 2113, 2117, 2201, 2203, 2205, 2206, 2208,
                2211, 2212, 2216, 2218, 2222, 2224, 2226, 2228, 2230, 2232, 2239, 2241, 2242, 2243, 2301, 2303, 2305, 2307,
                2309, 2312, 2314, 2316, 2321, 2323, 2325, 2328, 2330, 2332, 2335, 2338, 2340, 2341, 2352, 2401, 2403, 2405,
                2411, 2414, 2416, 2501, 2503, 2505, 2510, 2512, 2608, 2609, 2612, 2613, 2627, 2628, 5055, 5092, 5112, 5114,
                6095, 6096, 6097, 6098, 6099, 6100, 6104, 6105, 6106, 6107, 6108, 6109, 6110, 6111, 6112, 6113, 6114, 7033,
                7127, 7128, 7129, 7130, 7131, 7132, 7133, 7134, 7140, 7141, 7143, 7144, 7154, 7170, 7419, 7434, 7452, 7453,
                7454, 7455, 7456, 7457, 7482, 7521, 7522, 7523, 7524, 7821, 7822, 7823, 7824, 8830, 8834, 10001, 10011, 10012,
                10013, 10014, 10017, 10019, 10020, 10025, 10031, 10037, 10038, 11515, 11516, 13000, 13003, 13102, 13150, 13151,
                13155, 13163, 13165, 13168, 13200, 13201, 13202, 13208, 13209, 13210, 13211, 13250, 13251, 13252, 13253, 13254,
                20495 };

            // List is generated from Iela's List
            Cooking = new List<int>() { 501, 503, 505, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520, 521,
                522, 526, 531, 533, 534, 537, 539, 548, 549, 553, 567, 568, 576, 577, 578, 579, 580, 581, 582, 605, 606, 607, 608,
                609, 610, 619, 620, 621, 622, 632, 633, 634, 645, 702, 704, 705, 706, 707, 708, 709, 710, 711, 736, 903, 904, 908,
                918, 921, 929, 934, 936, 940, 942, 945, 948, 950, 951, 954, 956, 960, 961, 962, 966, 970, 1003, 1015, 1019, 1022,
                1023, 1024, 1028, 1033, 1037, 1042, 1046, 1050, 1054, 1061, 1062, 7003, 7006, 7031, 7066, 7098, 7100, 7119, 7123,
                7164, 7182, 7188, 7194, 7198, 7286, 7298, 7452, 7453, 7454, 7455, 7456, 7457, 7482 };

            // List is generated from Iela's List
            Dyestuffs = new List<int>() { 507, 508, 509, 510, 511, 713, 970, 971, 972, 973, 974 };

            // List is generated from Iela's List
            Forging = new List<int>() { 718, 719, 720, 722, 723, 724, 726, 727, 728, 729, 730, 733, 909, 913, 920, 922, 923, 957,
                958, 963, 968, 984, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1003 };

            // List is generated from Iela's List
            NewWorldFood = new List<int>() { 6020, 6021, 6023 };

            // List is generated from Iela's List
            Quest = new List<int>() { 503, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 517, 518, 519, 520, 521, 522,
                523, 526, 528, 529, 530, 531, 533, 535, 536, 537, 538, 539, 544, 547, 548, 549, 551, 567, 568, 574, 576, 578, 579,
                582, 602, 603, 604, 605, 607, 610, 611, 612, 613, 615, 619, 620, 621, 622, 624, 627, 633, 638, 660, 688, 690, 692,
                701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 717, 719, 721, 722, 724, 726, 727, 728,
                730, 731, 732, 733, 736, 737, 739, 740, 744, 746, 747, 748, 749, 756, 757, 901, 904, 905, 907, 908, 909, 910, 911,
                912, 914, 915, 916, 918, 919, 920, 921, 923, 925, 930, 931, 934, 936, 938, 939, 941, 943, 945, 948, 949, 952, 953,
                954, 955, 959, 964, 965, 966, 967, 968, 969, 970, 971, 973, 974, 975, 976, 978, 979, 980, 981, 982, 983, 984, 985,
                988, 989, 991, 992, 994, 995, 996, 998, 999, 1000, 1001, 1002, 1003, 1006, 1015, 1019, 1022, 1023, 1024, 1026, 1028,
                1029, 1030, 1032, 1033, 1034, 1036, 1037, 1038, 1039, 1042, 1048, 1049, 1052, 1053, 1054, 1059, 1061, 1067, 1092,
                1093, 1095, 1097, 1099, 1124, 1201, 1202, 1249, 1356, 1550, 1751, 1767, 1907, 1914, 2201, 2203, 2207, 2209, 2210,
                2211, 2212, 2213, 2214, 2221, 2226, 2227, 2229, 2233, 2236, 2239, 2243, 2244, 2247, 2248, 2249, 2250, 2252, 2253,
                2254, 2255, 2258, 2265, 2266, 2269, 2271, 2274, 2275, 2278, 2279, 2280, 2281, 2282, 2284, 2285, 2286, 2287, 2288,
                2292, 2294, 2299, 2406, 2407, 2608, 2611, 2613, 2656, 4001, 4052, 4065, 4097, 4100, 4107, 5001, 5009, 5010, 5015,
                5028, 5030, 5032, 5033, 5037, 5038, 5041, 5049, 5062, 5064, 5065, 5076, 5091, 5114, 5117, 5120, 5124, 5172, 6020,
                6023, 6032, 6041, 6073, 6075, 6086, 6090, 6264, 6265, 7001, 7003, 7012, 7013, 7015, 7027, 7030, 7031, 7032, 7033,
                7035, 7036, 7038, 7047, 7048, 7053, 7054, 7063, 7064, 7065, 7066, 7068, 7069, 7086, 7094, 7096, 7097, 7098, 7099,
                7100, 7101, 7106, 7107, 7108, 7111, 7112, 7113, 7116, 7118, 7120, 7121, 7122, 7123, 7137, 7140, 7142, 7143, 7150,
                7151, 7156, 7161, 7162, 7163, 7165, 7166, 7167, 7169, 7171, 7182, 7186, 7188, 7190, 7192, 7193, 7194, 7197, 7198,
                7200, 7201, 7203, 7205, 7206, 7211, 7213, 7215, 7216, 7217, 7220, 7263, 7264, 7267, 7270, 7286, 7290, 7292, 7293,
                7295, 7296, 7297, 7298, 7299, 7300, 7301, 7312, 7315, 7316, 7317, 7318, 7319, 7321, 7323, 7325, 7326, 7340, 7345,
                7347, 7352, 7353, 7354, 7355, 7433, 7440, 7441, 7445, 7446, 7447, 7448, 7507, 7510, 7511, 7561, 7563, 7564, 7939,
                8553, 8925, 8961, 9001, 9005, 10004, 10006, 10007, 10015, 10016, 10019, 11515, 11516, 12002, 12018, 12050, 12079,
                12082, 12095, 12127, 12353 };

            // List is generated from TRO wiki
            SQI = new List<int>() { 1190, 1290, 1320, 1430, 1530, 1549, 1590, 1650, 1651, 1745, 1746, 1840, 1913, 1990, 2150,
                2480, 2629, 2630, 5600, 8048, 8049, 13320 };

            // List is generated from TRO wiki
            SQIBonus = new List<int>() { 901, 903, 904, 914, 915, 916, 918, 920, 922, 924, 926, 928, 930, 932, 935,
                936, 938, 939, 940, 941, 942, 944, 947, 948, 949, 951, 953, 955, 956, 958, 961, 962, 967, 1000, 1012, 1018,
                1019, 1020, 1021, 1022, 1023, 1024, 1028, 1033, 1034, 1035, 1036, 1037, 1038, 1040, 1042, 1044, 1045, 1048,
                1053, 1054, 1056, 1057, 1063, 1064, 1067, 1068, 1096, 1098, 1099, 6020, 6021, 6023, 6032, 6041, 7001, 7002,
                7004, 7005, 7007, 7009, 7011, 7012, 7013, 7027, 7031, 7038, 7043, 7047, 7063, 7064, 7065, 7069, 7093, 7095,
                7096, 7097, 7098, 7099, 7100, 7107, 7110, 7112, 7116, 7117, 7118, 7120, 7121, 7122, 7123, 7125, 7149, 7150,
                7151, 7155, 7156, 7158, 7159, 7162, 7167, 7171, 7172, 7186, 7187, 7188, 7189, 7190, 7192, 7193, 7194, 7195,
                7196, 7197, 7200, 7202, 7203, 7207, 7208, 7209, 7210, 7212, 7213, 7214, 7215, 7216, 7217, 7219, 7220, 7222,
                7223, 7225, 7263, 7264, 7265, 7266, 7269, 7270, 7286, 7298, 7299, 7301, 7312, 7315, 7317, 7318, 7319, 7321,
                7322, 7323, 7325, 7340, 7347, 7507, 7561, 7564, 7565, 7567, 7751, 7752, 7753, 7762, 7799 };

            // List is generated from TRO wiki
            SQIIngredient = new List<int>() { 605, 609, 678, 687, 689, 691, 693, 695, 707, 714, 731, 968, 970, 984,
                985, 989, 990, 991, 992, 994, 995, 996, 997, 1006, 1007, 1009, 1097, 1139, 1237, 1244, 1261, 1270, 1304,
                1306, 1370, 1376, 1411, 1413, 1472, 1473, 1483, 1524, 1529, 1552, 1553, 1554, 1555, 1556, 1557, 1613, 1614,
                1615, 1620, 1629, 1719, 1720, 1722, 1732, 1766, 1802, 1814, 1902, 1906, 1908, 1912, 1964, 1980, 2108, 2109,
                2111, 2116, 2254, 2258, 2327, 2355, 2404, 2406, 2410, 2412, 2420, 2421, 2521, 2530, 2601, 2614, 2615, 2619,
                2621, 2622, 2623, 2624, 2625, 2626, 2627, 2650, 2659, 2660, 2661, 2662, 4008, 4019, 4024, 4028, 4032, 4035,
                4043, 4057, 4058, 4063, 4066, 4067, 4072, 4075, 4079, 4084, 4091, 4101, 4102, 4114, 4122, 4124, 4246, 4264,
                4303, 4315, 4322, 4382, 4390, 4423, 4443, 4450, 5001, 5017, 5025, 5081, 5093, 5096, 5125, 6089, 6090, 7017,
                7018, 7019, 7020, 7023, 7036, 7048, 7094, 7108, 7158, 7289, 7300, 7440, 7441, 7445, 7446, 7447, 7448, 7510,
                7566, 8008, 8061, 9021, 9022, 10006, 12020, 13300, 13302, 13303, 20200, 20201, 20202, 20203, 20204, 20205,
                20206, 20207, 20208, 20209, 20210, 20211, 20212, 20213, 20214, 20215, 20216, 20217 };

            // List is generated from Iela's List
            SeasonalEvent = new List<int>() { 508, 520, 521, 523, 526, 538, 539, 549, 568, 578, 582, 605, 609, 620, 702, 703,
                704, 720, 733, 744, 901, 914, 916, 934, 949, 951, 952, 953, 961, 966, 980, 990, 991, 994, 1000, 1001, 1003,
                1013, 1021, 1026, 1034, 1040, 1066, 1096, 2257, 2284, 2504, 2528, 2611, 6256, 6264, 6265, 6405, 6509, 6510,
                6511, 7013, 7063, 7066, 7071, 7097, 7098, 7100, 7112, 7126, 7149, 7164, 7166, 7182, 7189, 7194, 7198, 7205,
                7213, 7216, 7220, 7225, 7264, 7315, 7347, 7354, 7440, 7445, 7446, 7447, 7448, 7449, 7561, 7563, 7938, 8890,
                8891, 8892, 8898, 8899, 8912, 8913, 8914, 12359, 20058 };

            // List is generated from Iela's List
            SkillRequirement = new List<int>() { 657, 713,715, 716, 717, 756, 904, 937, 939, 946, 947, 952, 972, 990, 991, 992,
                993, 998,999, 1002, 1013, 7033, 7433 };
        }

        private static void CreateInputBackup( string inputFile )
        {
            string ext = Path.GetExtension( inputFile );
            string fileWithoutExt = Path.GetFileNameWithoutExtension( inputFile );
            string BackupDirectory = Path.Combine( BaseDirectory, "Backup" );
            string backupFile = Path.Combine( BackupDirectory, fileWithoutExt + "_backup_" + DateTime.Now.ToString( "yyyy_MM_dd_HH_mm_ss" ) + ext );

            if( !Directory.Exists( BackupDirectory ) )
            {
                Console.WriteLine( "Created backup folder." );
                Directory.CreateDirectory( BackupDirectory );
            }

            // A file with the same name shouldn't exist but just to be sure
            if( File.Exists( backupFile ) )
            {
                File.Delete( backupFile );
            }

            File.Copy( inputFile, backupFile );
        }

        private static List<string> RemoveExistingInfo( List<string> input )
        {
            var output = new List<string>();
            var existingInfo = new List<string>() { OldBuyableInfo, OldSQIIngredientInfo,OldNewBuyableInfo, NewArrowCraftingInfo, NewBrewingInfo, NewCookingInfo,
                NewBuyableInfo, NewDyestuffsInfo, NewForgingInfo, NewNewWorldFoodInfo, NewQuestInfo, NewSQIInfo, NewSQIBonusInfo, NewSQIIngredientInfo,
                NewSeasonalEventInfo, NewSkillRequirementInfo };

            int removedInfos = 0;

            foreach( string inputLine in input )
            {
                if( existingInfo.Contains( inputLine ) )
                {
                    removedInfos++;
                    continue;
                }

                output.Add( inputLine );
            }

            Console.WriteLine();
            Console.WriteLine( " - Removed: " + removedInfos );
            Console.WriteLine();

            return output;
        }

        private static List<string> AddNewInfo( List<string> input )
        {
            var output = new List<string>();
            var trimChar = '#';
            var seperator = '\n';
            int tryParseOutput;
            int itemID = 0;
            int itemCount = 0;

            int addedArrowCrafting = 0;
            int addedBrewing = 0;
            int addedBuyables = 0;
            int addedCooking = 0;
            int addedDyeable = 0;
            int addedForging = 0;
            int addedNewWorldFood = 0;
            int addedQuestItem = 0;
            int addedSQI = 0;
            int addedSQIBonus = 0;
            int addedSQIIngredients = 0;
            int addedSeasonalEvent = 0;
            int addedSkillRequirement = 0;

            foreach( string inputLine in input )
            {

                if( int.TryParse( inputLine.TrimEnd( trimChar ), out tryParseOutput ) )
                {
                    itemCount++;
                    itemID = tryParseOutput;

                    output.Add( inputLine );

                    string lineToAdd = string.Empty;
                    int addedInfo = 0;

                    if( addSQIIngredientInfo && SQIIngredient.Contains( itemID ) )
                    {
                        lineToAdd += NewSQIIngredientInfo + " ";
                        addedInfo += 2;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedSQIIngredients++;
                    }
                    if( addNewWorldFoodInfo && NewWorldFood.Contains( itemID ) )
                    {
                        lineToAdd += NewNewWorldFoodInfo + " ";
                        addedInfo += 2;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedNewWorldFood++;
                    }
                    if( addSQIBonusInfo && SQIBonus.Contains( itemID ) )
                    {
                        lineToAdd += NewSQIBonusInfo + " ";
                        addedInfo++;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedSQIBonus++;
                    }
                    if( addBuyableInfo && Buyable.Contains( itemID ) )
                    {
                        lineToAdd += NewBuyableInfo + " ";
                        addedInfo++;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedBuyables++;
                    }
                    if( addArrowCraftingInfo && ArrowCrafting.Contains( itemID ) )
                    {
                        lineToAdd += NewArrowCraftingInfo + " ";
                        addedInfo++;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedArrowCrafting++;
                    }
                    if( addBrewingInfo && Brewing.Contains( itemID ) )
                    {
                        lineToAdd += NewBrewingInfo + " ";
                        addedInfo++;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedBrewing++;
                    }
                    if( addCookingInfo && Cooking.Contains( itemID ) )
                    {
                        lineToAdd += NewCookingInfo + " ";
                        addedInfo++;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedCooking++;
                    }
                    if( addDyestuffInfo && Dyestuffs.Contains( itemID ) )
                    {
                        lineToAdd += NewDyestuffsInfo + " ";
                        addedInfo++;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedDyeable++;
                    }
                    if( addForgingInfo && Forging.Contains( itemID ) )
                    {
                        lineToAdd += NewForgingInfo + " ";
                        addedInfo++;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedForging++;
                    }
                    if( addQuestItemInfo && Quest.Contains( itemID ) )
                    {
                        lineToAdd += NewQuestInfo + " ";
                        addedInfo++;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedQuestItem++;
                    }
                    if( addSQIInfo && SQI.Contains( itemID ) )
                    {
                        lineToAdd += NewSQIInfo + " ";
                        addedInfo++;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedSQI++;
                    }
                    if( addSeasonalInfo && SeasonalEvent.Contains( itemID ) )
                    {
                        lineToAdd += NewSeasonalEventInfo + " ";
                        addedInfo++;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedSeasonalEvent++;
                    }
                    if( addSkillRequirementInfo && SkillRequirement.Contains( itemID ) )
                    {
                        lineToAdd += NewSkillRequirementInfo + " ";
                        addedInfo++;
                        if( ( addedInfo % 2 ) == 0 )
                        {
                            lineToAdd += seperator;
                        }
                        addedSkillRequirement++;
                    }

                    // split into multiple lines
                    var lines = lineToAdd.Split( seperator );
                    foreach( string line in lines )
                    {
                        if( !string.IsNullOrWhiteSpace( line ) )
                        {
                            output.Add( line );
                        }
                    }
                }
                else
                {
                    output.Add( inputLine );
                }
            }

            int totalAdded = addedArrowCrafting + addedBrewing + addedBuyables + addedCooking + addedDyeable + addedForging + addedNewWorldFood
                + addedQuestItem + addedSQI + addedSQIBonus + addedSQIIngredients + addedSeasonalEvent + addedSkillRequirement;

            int totalIDs = ArrowCrafting.Count + Brewing.Count + Buyable.Count + Cooking.Count + Dyestuffs.Count + Forging.Count
                + NewWorldFood.Count + Quest.Count + SQI.Count + SQIBonus.Count + SQIIngredient.Count + SeasonalEvent.Count
                + SkillRequirement.Count;

            const int Pad = 3;

            Console.WriteLine();
            Console.WriteLine( " - Item Count: " + itemCount );
            Console.WriteLine();
            Console.WriteLine( " - Added " + addedArrowCrafting.ToString().PadLeft( Pad ) + " / " + ArrowCrafting.Count.ToString().PadLeft( Pad ) + " Arrow Crafting Info" );
            Console.WriteLine( " - Added " + addedBrewing.ToString().PadLeft( Pad ) + " / " + Brewing.Count.ToString().PadLeft( Pad ) + " Brewing Info" );
            Console.WriteLine( " - Added " + addedBuyables.ToString().PadLeft( Pad ) + " / " + Buyable.Count.ToString().PadLeft( Pad ) + " Buyable Info" );
            Console.WriteLine( " - Added " + addedCooking.ToString().PadLeft( Pad ) + " / " + Cooking.Count.ToString().PadLeft( Pad ) + " Cooking Info" );
            Console.WriteLine( " - Added " + addedDyeable.ToString().PadLeft( Pad ) + " / " + Dyestuffs.Count.ToString().PadLeft( Pad ) + " Dyestuff Info" );
            Console.WriteLine( " - Added " + addedForging.ToString().PadLeft( Pad ) + " / " + Forging.Count.ToString().PadLeft( Pad ) + " Forging Info" );
            Console.WriteLine( " - Added " + addedNewWorldFood.ToString().PadLeft( Pad ) + " / " + NewWorldFood.Count.ToString().PadLeft( Pad ) + " New World Food Info" );
            Console.WriteLine( " - Added " + addedQuestItem.ToString().PadLeft( Pad ) + " / " + Quest.Count.ToString().PadLeft( Pad ) + " Quest Item Info" );
            Console.WriteLine( " - Added " + addedSQI.ToString().PadLeft( Pad ) + " / " + SQI.Count.ToString().PadLeft( Pad ) + " SQI Info" );
            Console.WriteLine( " - Added " + addedSQIBonus.ToString().PadLeft( Pad ) + " / " + SQIBonus.Count.ToString().PadLeft( Pad ) + " SQI Bonus Info" );
            Console.WriteLine( " - Added " + addedSQIIngredients.ToString().PadLeft( Pad ) + " / " + SQIIngredient.Count.ToString().PadLeft( Pad ) + " SQI Ingredient Info" );
            Console.WriteLine( " - Added " + addedSeasonalEvent.ToString().PadLeft( Pad ) + " / " + SeasonalEvent.Count.ToString().PadLeft( Pad ) + " Seasonal Event Info" );
            Console.WriteLine( " - Added " + addedSkillRequirement.ToString().PadLeft( Pad ) + " / " + SkillRequirement.Count.ToString().PadLeft( Pad ) + " Skill Requirement Info" );
            Console.WriteLine();
            Console.WriteLine( " - Total: " + totalAdded + " / " + totalIDs );
            Console.WriteLine();
            Console.WriteLine( " - Input lines: " + input.Count );
            Console.WriteLine( " - Output lines: " + output.Count );
            Console.WriteLine();

            return output;
        }
    }
}
